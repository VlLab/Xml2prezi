# Xml2prezi

## Description

«XML-презентации»

## LaTeX

На Github уравнения не визуализируются простым способом $$x=\sqrt{2}$$ Однако
есть несоколько возможностей вставить уравнения:

-   Как внедрение с сайта (через URL Encoded)
    ![equation](https://latex.codecogs.com/gif.latex?e%5E%7Bi%20%5Cpi%7D%20%3D%20-1)

-   Как изображение

## TODO

-   LaTeX в README.md

## History

-   \* 05.08.2021 21:17 добавил копию на GitLab, вызов git push all

-   01.08.2021 Тест git acppa

-   19.12.2020 LaTeX в README.md

-   11.12.2020 v.1.0.1 Initial Release

-   После студийной записи Вводной лекции по НЭП

## Autors

-   Evtikhov V. evg2002@yandex.ru

-   Evtikhova N. env649@yandex.ru

-   Evtikhov M. emv649@yandex.ru

## Acknowledgments

-   [ Inkscape — wiki34](http://wiki.netev.mykeenetic.com/index.php/Inkscape)

## License

This project is licensed under the ©EVG License - see the LICENSE.md file for
details
